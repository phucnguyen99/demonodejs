const { render } = require("ejs")

import db from "../models/index"

import CRUDServices from "../services/CRUDServices";

let getHomePage = async (req, res) => {
    try {
        let data = await db.User.findAll();
        console.log(data)

        return res.render('homePage.ejs', {
            data: JSON.stringify(data)
        })
    } catch (e) {

        console.log(e)
    }

}

let getAboutPage = (req, res) => {
    return res.render('text/about.ejs')
}

let getCRUD = (req, res) => {
    return res.render('crud.ejs')
}

let postCRUD = async (req, res) => {
    let message = await CRUDServices.createUser(req.body);
    console.log(message)
    return res.send('post method from server')
}

let displayGetCRUD = async (req, res) => {
    let users = await CRUDServices.getAllUser();
    return res.render('displayCRUD.ejs', {
        dataTable: users
    })

}
let getForEditCRUD = async (req, res) => {
    let userId = req.query.id
    if (userId) {
        let userData = await CRUDServices.getUserById(userId)

        return res.render('editCRUD.ejs', {
            userData: userData
        })

    } else {
        return res.send('not found')

    }
}

let putCRUD = async (req, res) => {
    let data = req.body;
    let allUsers = await CRUDServices.updateUser(data);
    return res.render('displayCRUD.ejs', {
        dataTable: allUsers
    })
}

let deleteCRUD = async (req, res) => {
    let userId = req.query.id;
    if (userId) {
        await CRUDServices.deleteUserById(userId);
        return res.send('Delete User Succeed')
    }
}


module.exports = {
    getHomePage: getHomePage,
    getAboutPage: getAboutPage,
    getCRUD: getCRUD,
    postCRUD: postCRUD,
    displayGetCRUD: displayGetCRUD,
    getForEditCRUD: getForEditCRUD,
    putCRUD: putCRUD,
    deleteCRUD: deleteCRUD
}