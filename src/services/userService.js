import db from "../models/index";

import bcrypt from "bcryptjs";

var salt = bcrypt.genSaltSync(10);

let hashUserPassword = (password) => {
    return new Promise(async (resolve, reject) => {
        try {
            let hashPassword = await bcrypt.hashSync(password, salt);
            resolve(hashPassword);
        } catch (error) {
            reject(error)
        }

    })

}

let handleLogin = (email, password) => {
    return new Promise(async (resolve, reject) => {
        try {
            let userData = {}
            let isExist = await checkUserEmail(email)
            if (isExist) {

                // continue compare password
                let user = await db.User.findOne({
                    attributes: ['email', 'roleId', 'password'],
                    where: { email: email },
                    raw: true
                })
                if (user) {

                    // compare password

                    let isValidPassword = bcrypt.compareSync(password, user.password); // false
                    if (isValidPassword) {
                        userData.errCode = 0
                        userData.errMessage = 'OK'
                        delete user.password;
                        userData.user = user

                    } else {
                        userData.errCode = 4
                        userData.errMessage = 'password is not correct'
                    }


                } else {
                    userData.errCode = 2
                    userData.errMessage = "user not found"
                }
            } else {
                userData.errCode = 1
                userData.errMessage = "Your email not exist in system, pls try again!"
            }
            resolve(userData)


        } catch (error) {
            reject(error)
        }
    })
}

let checkUserEmail = (userEmail) => {
    return new Promise(async (resolve, reject) => {
        try {
            let user = await db.User.findOne({
                where: { email: userEmail }
            })

            if (user) {
                resolve(true)
            } else {
                resolve(false)
            }

        } catch (error) {
            reject(error)
        }
    })
}

let getAllUsers = (userId) => {
    return new Promise(async (resolve, reject) => {
        try {
            let users = '';
            if (userId === 'ALL') {
                users = await db.User.findAll({
                    attributes: {
                        exclude: ['password']
                    }
                })
            }
            if (userId && userId !== 'ALL') {
                users = await db.User.findOne({
                    where: { id: userId },
                    attributes: {
                        exclude: ['password']
                    }
                })
            }
            resolve(users)
        } catch (error) {
            reject(error)
        }
    })
}
let createNewUser = (data) => {

    return new Promise(async (resolve, reject) => {
        try {
            // first check email exist
            let isEmailExist = await checkUserEmail(data.email)
            if (isEmailExist === true) {
                resolve({
                    errCode: 1,
                    errMessage: 'Your email is exist, please try another email!'
                })

            } else {
                let hashPasswordFromBcrypt = await hashUserPassword(data.password);
                await db.User.create({
                    email: data.email,
                    password: hashPasswordFromBcrypt,
                    firstName: data.firstName,
                    lastName: data.lastName,
                    address: data.address,
                    phoneNumber: data.phoneNumber,
                    gender: data.gender === '1' ? true : false,
                    roleId: data.roleId
                })

                resolve({
                    errCode: 0,
                    message: 'OK'
                })
            }


        } catch (error) {
            reject(error)
        }
    })
}

let deleteUser = (userId) => {

    return new Promise(async (resolve, reject) => {
        try {
            let user = await db.User.findOne({
                where: { id: userId }
            })
            if (!user) {
                resolve({
                    errCode: 2,
                    message: 'The user is not exist'
                })
            }
            await db.User.destroy({
                where: { id: userId }
            })
        } catch (error) {
            reject(error)
        }
    })
}

let updateUser = (data) => {

    return new Promise(async (resolve, reject) => {
        try {
            if (!data.id) {
                resolve({
                    errCode: 2,
                    errMessage: 'Missing params'
                })
            }
            let user = await db.User.findOne({
                where: { id: data.id },
                raw: false
            })
            if (user) {
                user.firstName = data.firstName;
                user.lastName = data.lastName;
                user.address = data.address;


                await user.save()

                resolve({
                    errCode: 0,
                    message: 'Update user succeed'
                })
            } else {
                resolve({
                    errCode: 0,
                    errMessage: 'User not found'
                })
            }
        } catch (error) {
            reject(error)
        }
    })
}



module.exports = {
    handleLogin: handleLogin,
    getAllUsers: getAllUsers,
    createNewUser: createNewUser,
    updateUser: updateUser,
    deleteUser: deleteUser
}